﻿** Settings ***
Library         Collections
Library         String
Library         RequestsLibrary
Library         json
Library         JSONLibrary
Library         OperatingSystem
Library         DateTime


*** Variables ***
${base_url}              https://order-pizza-api.herokuapp.com


*** Test Cases ***
PREPARE_DATA
    ${ORDER}        Generate random string    2    0123456789
    ${TABLE}        Generate random string    1    0123456789
    ${DATE} =       Get Current Date
    Set Global Variable                          ${ORDER}
    Set Global Variable                          ${TABLE}
    Set Global Variable                          ${DATE}


TC1_POST_AUTH
    Create Session      post_auth               ${base_url}         verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${body}=            Create dictionary       username=test       password=test
    ${response}=        POST On Session         post_auth           /api/auth       headers=${headers}      json=${body}
    ${response.status_code}                     Convert To String                           ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    #log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}                     200
    ${accessToken}=                             evaluate    $response.json().get("access_token")
    Set Global Variable                         ${accessToken}


TC2_GET_REQUEST
    Create Session      get_order               ${base_url}         verify=True
    ${response}=        GET On Session          get_order                               /api/orders
    ${response.status_code}                     Convert To String                       ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    #log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}                 200


TC3_POST_REQUEST
    Create Session      post_order              ${base_url}         verify=True
    ${body}=            create dictionary       "Crust": "pizza"   "Flavor": "spicy"  "Order_ID": ${ORDER}   "Size": "XL"  "Table_No": ${TABLE}   "Timestamp": "${DATE}"
    ${headers}=         create dictionary       Authorization=Bearer ${accessToken}      Accept=application/json     Content-Type=application/json
    ${response}=        POST On Session         post_order      /api/orders     headers=${headers}      json=${body}
    ${response.status_code}                     Convert To String               ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    #log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    #should be equal                             ${response.status_code}             201

TC4_DELETE_REQUEST
    Create Session      delete_order            ${base_url}         verify=True
    ${response}=        DELETE On Session       delete_order                           /api/orders/${ORDER}
    ${response.status_code}                     Convert To String                       ${response.status_code}
    log to console                              \nstatus_code : ${response.status_code}
    #log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    #should be equal                             ${response.status_code}                 200